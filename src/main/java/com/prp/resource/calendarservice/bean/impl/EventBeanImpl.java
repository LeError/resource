/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.bean.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.calendarservice.bean.CalendarBean;
import com.prp.resource.calendarservice.bean.EventBean;
import com.prp.resource.calendarservice.dal.CalendarDAL;
import com.prp.resource.calendarservice.dal.EventDAL;
import com.prp.resource.calendarservice.dal.model.CalendarPermissionDB;
import com.prp.resource.calendarservice.dal.model.EventDB;
import com.prp.resource.calendarservice.dal.model.ReminderDB;
import com.prp.resource.calendarservice.util.EventConverter;
import com.prp.resource.common.enums.calendar.CalendarPermissions;
import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.calendar.Event;
import com.prp.resource.common.service.UserService;

/**
 * @see EventBean
 * @author Eric Fischer
 *
 */

@Component
public class EventBeanImpl implements EventBean {

	private static final Logger log = LoggerFactory.getLogger(EventBeanImpl.class);

	private EventDAL dal;
	private UserService userService;
	private EventConverter converter;
	private CalendarBean calendarBean;
	private CalendarDAL calendarDal;

	@Autowired
	public EventBeanImpl(final EventDAL dal, final UserService userService,
			final EventConverter converter, final CalendarBean calendarBean,
			final CalendarDAL calendarDal) {
		this.dal = dal;
		this.userService = userService;
		this.converter = converter;
		this.calendarBean = calendarBean;
		this.calendarDal = calendarDal;
	}

	@Override
	public List<Event> loadEventsForDay(final Date day) throws PRPException {
		if (day == null) {
			String message = "No day for search given.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_PARAMETERS_MISSING);
		}
		Long currentUserId = userService.loadCurrentUser().getId();
		List<EventDB> events = dal.loadEventsForDay(currentUserId, day);
		List<Event> retVal = new ArrayList<>();
		for (EventDB event : events) {
			Set<CalendarPermissionDB> permissions = event.getCalendar().getPermissions();
			CalendarPermissionDB userPermission = permissions.stream()
					.filter(p -> p.getUserId().equals(currentUserId)).collect(Collectors.toList())
					.get(0);
			if (userPermission.getAccessLevel().equals(CalendarPermissionDB.BLOCKED_ONLY)) {
				log.debug("Converting event on BLOCKED_ONLY level.");
				Event blockedOnly = new Event();
				blockedOnly.setId(event.getId());
				blockedOnly.setCreatorId(event.getCreatorsUserId());
				retVal.add(blockedOnly);
			} else {
				retVal.add(converter.convertEvent(event));
			}
		}
		return retVal;
	}

	@Override
	public List<Event> loadEventsForCurrentUser() throws PRPException {
		List<EventDB> loadedEvents = dal.loadEventsForUser(userService.loadCurrentUser().getId());

		List<Event> retVal = new ArrayList<>();

		for (EventDB event : loadedEvents) {
			retVal.add(converter.convertEvent(event));
		}

		return retVal;
	}

	@Override
	public Event addEvent(final Event event) throws PRPException {
		if (event == null || event.getCalendar() == null || event.getCalendar().getId() == null
				|| event.getCalendar().getId().equals(Long.valueOf(0))) {
			String message = "No event or event details given.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_PARAMETERS_MISSING);
		}
		checkStoringInput(event);

		EventDB toStore = new EventDB();
		toStore.setTitle(event.getTitle());
		toStore.setDescription(event.getDescription());
		toStore.setStartDate(event.getStartDate());
		toStore.setEndDate(event.getEndDate());
		toStore.setLocation(event.getLocation());
		toStore.setCreatorsUserId(userService.loadCurrentUser().getId());
		toStore.setCalendar(calendarDal.loadCalendar(event.getCalendar().getId()));

		EventDB stored = dal.storeEvent(toStore);

		for (Date reminder : event.getReminders()) {
			addReminder(stored.getId(), reminder);
		}

		stored.setReminders(new HashSet<>(dal.loadReminders(stored.getId())));

		return converter.convertEvent(stored);
	}

	@Override
	public Event updateEvent(final Event event) throws PRPException {
		if (event == null || event.getId() == null || event.getId().equals(Long.valueOf(0))) {
			String message = "No event or event details given.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_PARAMETERS_MISSING);
		}
		checkStoringInput(event);

		EventDB eventDB = dal.loadSingleEvent(event.getId());
		if (eventDB == null) {
			throw new PRPException(String.format("No event for ID %d found.", event.getId()),
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_NO_EVENT_FOR_ID_FOUND);
		}
		eventDB.setTitle(event.getTitle());
		eventDB.setDescription(event.getDescription());
		eventDB.setStartDate(event.getStartDate());
		eventDB.setEndDate(event.getEndDate());
		eventDB.setLocation(event.getLocation());
		List<ReminderDB> keepOrAdd = new ArrayList<>();
		for (Date reminderDate : event.getReminders()) {
			List<ReminderDB> found = eventDB.getReminders().stream()
					.filter(p -> p.getRemindTime().equals(reminderDate))
					.collect(Collectors.toList());
			if (!found.isEmpty()) {
				keepOrAdd.addAll(found);
			} else {
				ReminderDB newReminder = new ReminderDB();
				newReminder.setEvent(eventDB);
				newReminder.setRemindTime(reminderDate);
				keepOrAdd.add(dal.addReminder(eventDB.getId(), reminderDate));
			}
		}
		for (ReminderDB storedReminder : eventDB.getReminders()) {
			if (!keepOrAdd.contains(storedReminder)) {
				dal.deleteReminder(eventDB.getId(), storedReminder.getRemindTime());
			}
		}
		eventDB.getReminders().clear();
		eventDB.getReminders().addAll(keepOrAdd);

		EventDB updatedEvent = dal.updateEvent(eventDB);

		return converter.convertEvent(updatedEvent);
	}

	@Override
	public void deleteEvent(final Long eventId) throws PRPException {
		EventDB loaded = dal.loadSingleEvent(eventId);
		checkEditingPermissions(loaded.getCalendar().getId());
		dal.deleteEvent(eventId);
	}

	@Override
	public Event addReminder(final Long eventId, final Date reminderTime) throws PRPException {
		EventDB loaded = dal.loadSingleEvent(eventId);
		if (loaded == null) {
			String msg = String.format("Event %d not found to add reminder.", eventId);
			log.error(msg);
			throw new PRPException(msg,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_NO_EVENT_FOR_ID_FOUND);
		}
		checkEditingPermissions(loaded.getCalendar().getId());
		dal.addReminder(eventId, reminderTime);
		return converter.convertEvent(dal.loadSingleEvent(eventId));
	}

	@Override
	public void deleteReminder(final Long eventId, final Date reminderTime) throws PRPException {
		EventDB loaded = dal.loadSingleEvent(eventId);
		if (loaded == null) {
			String msg = String.format("Event %d not found to delete reminder.", eventId);
			log.error(msg);
			throw new PRPException(msg,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_NO_EVENT_FOR_ID_FOUND);
		}
		checkEditingPermissions(loaded.getCalendar().getId());
		dal.deleteReminder(eventId, reminderTime);
	}

	/**
	 * Checks the validity of the input parameter and the user's access rights.
	 *
	 * @param event
	 *            - {@link Event} to check.
	 * @throws PRPException
	 */
	private void checkStoringInput(final Event event) throws PRPException {
		Calendar start = new GregorianCalendar();
		start.setTime(event.getStartDate());
		Calendar end = new GregorianCalendar();
		end.setTime(event.getEndDate());

		if (end.before(start)) {
			String message = "End date cannot be before start date.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_END_BEFORE_START);
		}

		checkEditingPermissions(event.getCalendar().getId());
	}

	private void checkEditingPermissions(final Long calendarId) throws PRPException {
		com.prp.resource.common.model.calendar.Calendar foundCalendar = calendarBean
				.loadCalendar(calendarId);
		if (foundCalendar == null) {
			String msg = String.format("No calendar found for ID %d", calendarId);
			log.error(msg);
			throw new PRPException(msg,
					PRPErrorCode.BACKEND_CALENDARSERVICE_DAL_NO_CALENDAR_FOR_ID_FOUND);
		}
		CalendarPermissions permission = foundCalendar.getPermissions()
				.get(userService.loadCurrentUser().getId());
		CalendarPermissions publicPermissions = foundCalendar.getPermissions().get(null);

		if ((CalendarPermissions.BLOCKED_ONLY.equals(permission)
				|| CalendarPermissions.ALL_DETAILS.equals(permission)
				|| CalendarPermissions.EXCLUDE.equals(permission))
				&& !(CalendarPermissions.EDIT.equals(publicPermissions)
						|| CalendarPermissions.ADMIN.equals(publicPermissions))) {
			String message = "User hasn't access rights to write that calendar.";
			log.error(message);
			throw new PRPException(message,
					PRPErrorCode.BACKEND_CALENDARSERVICE_BEAN_USER_HAS_NO_PERMISSION);
		}
	}

}
