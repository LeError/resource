/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.calendarservice.dal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.prp.resource.calendarservice.dal.repository.CalendarPermissionsRepository;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

/**
 * Representing information about calendar permissions.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see CalendarPermissionsRepository
 * @see CalendarDB
 *
 */
@Entity
@Table(name = "lia_backend_calendarservice_calendaraccess", uniqueConstraints = @UniqueConstraint(columnNames = {
		"calendar", "userId" }))
@Data
public class CalendarPermissionDB {

	public static final String OWNER = "owner";
	public static final String ADMIN = "admin";
	public static final String EDIT = "edit";
	public static final String ALL_DETAILS = "details";
	public static final String BLOCKED_ONLY = "blocked";

	@Id
	@GeneratedValue
	@Setter(AccessLevel.NONE)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "calendar", nullable = false)
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	private CalendarDB calendar;

	@Column(name = "userid", nullable = true, unique = false)
	private Long userId;

	@Column(name = "accesslevel", nullable = false, unique = false)
	private String accessLevel;

	@Column(name = "deleted", nullable = false)
	private Boolean deleted = Boolean.FALSE;

	public void setAccessLevel(final String accessLevel) {
		if (accessLevel != null && (accessLevel.equals(ADMIN) || accessLevel.equals(ALL_DETAILS)
				|| accessLevel.equals(EDIT) || accessLevel.equals(BLOCKED_ONLY) || OWNER.equals(accessLevel))) {
			this.accessLevel = accessLevel;
		} else {
			throw new IllegalArgumentException(
					"Access level not valid. Choose one of the options declared within the permission database representation.");
		}
	}

}
