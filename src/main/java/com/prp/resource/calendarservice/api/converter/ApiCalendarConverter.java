package com.prp.resource.calendarservice.api.converter;

import org.springframework.stereotype.Component;

import com.prp.resource.calendarservice.api.model.ApiCalendar;
import com.prp.resource.common.enums.calendar.CalendarPermissions;
import com.prp.resource.common.model.calendar.Calendar;

@Component
public class ApiCalendarConverter {

	public ApiCalendar convert(Calendar in) {
		ApiCalendar out = new ApiCalendar();
		out.setId(in.getId());
		out.setName(in.getName());
		out.setDescription(in.getDescription());
		for (Long permissionUser : in.getPermissions().keySet()) {
			out.getPermissions().put(permissionUser,
					in.getPermissions().get(permissionUser).toString());
		}
		return out;
	}

	public Calendar convert(ApiCalendar in) {
		Calendar out = new Calendar();
		out.setId(in.getId());
		out.setName(in.getName());
		out.setDescription(in.getDescription());
		in.getPermissions().entrySet().stream().forEach((entry) -> {
			out.getPermissions().put(entry.getKey(),
					CalendarPermissions.getEnumCase(entry.getValue()));
		});
		return out;
	}

}
