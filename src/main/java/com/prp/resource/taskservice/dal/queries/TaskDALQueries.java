/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal.queries;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.prp.resource.taskservice.dal.TaskDAL;
import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskListDB;
import com.prp.resource.taskservice.dal.repository.TaskRepository;

/**
 * Queries for the {@link TaskDAL}.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskDAL
 * @see TaskDB
 * @see TaskRepository
 *
 */
@Component
public class TaskDALQueries {

	private static final String OWNER_ID = "ownerId";
	private static final String TASK_LIST = "taskList";
	private static final String DONE = "done";
	private static final String DELETED = "deleted";
	private static final String DUE_DATE = "dueDate";
	private static final String START_DATE = "startDate";

	public Specification<TaskDB> belongsTo(final TaskListDB taskList) {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = 3227521171410011560L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.equal(root.get(TASK_LIST), taskList);
				return pred;
			}

		};
	}

	public Specification<TaskDB> ownedBy(final Long userId) {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = -3768287279234229058L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.equal(root.get(TASK_LIST).get(OWNER_ID), userId);
				return pred;
			}

		};
	}

	public Specification<TaskDB> before(final Date date) {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = -3375616964408608014L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.lessThanOrEqualTo(root.get(START_DATE), date);
				return pred;
			}

		};
	}

	public Specification<TaskDB> after(final Date date) {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = 3259537488881784468L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.greaterThanOrEqualTo(root.get(START_DATE), date);
				return pred;
			}

		};
	}

	public Specification<TaskDB> dueBefore(final Date date) {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = 8350240912821146922L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.lessThanOrEqualTo(root.get(DUE_DATE), date);
				return pred;
			}

		};
	}

	public Specification<TaskDB> dueAfter(final Date date) {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = 4402589929919754994L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.greaterThanOrEqualTo(root.get(DUE_DATE), date);
				return pred;
			}

		};
	}

	public Specification<TaskDB> overdue(final Date dueDate) {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = -7592784218653727124L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.lessThanOrEqualTo(root.get(DUE_DATE), dueDate);
				return pred;
			}

		};
	}

	public Specification<TaskDB> noStartDate() {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = 3077599906185857052L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.isNull(root.get(START_DATE));
				return pred;
			}

		};
	}

	public Specification<TaskDB> noDueDate() {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = 7925302166923130022L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.isNull(root.get(DUE_DATE));
				return pred;
			}

		};
	}

	public Specification<TaskDB> notDeleted() {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = 1296389408395280228L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.not(root.get(DELETED));
				return pred;
			}

		};
	}

	public Specification<TaskDB> notDone() {
		return new Specification<TaskDB>() {

			private static final long serialVersionUID = -1541917651255955689L;

			@Override
			public Predicate toPredicate(final Root<TaskDB> root, final CriteriaQuery<?> query,
					final CriteriaBuilder criteriaBuilder) {
				Predicate pred = criteriaBuilder.not(root.get(DONE));
				return pred;
			}

		};
	}

}
