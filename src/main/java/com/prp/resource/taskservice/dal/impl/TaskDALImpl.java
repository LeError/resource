/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.taskservice.dal.TaskDAL;
import com.prp.resource.taskservice.dal.model.TaskDB;
import com.prp.resource.taskservice.dal.model.TaskListDB;
import com.prp.resource.taskservice.dal.model.TaskRelationDB;
import com.prp.resource.taskservice.dal.model.TaskReminderDB;
import com.prp.resource.taskservice.dal.queries.TaskDALQueries;
import com.prp.resource.taskservice.dal.repository.TaskListRepository;
import com.prp.resource.taskservice.dal.repository.TaskRelationRepository;
import com.prp.resource.taskservice.dal.repository.TaskReminderRepository;
import com.prp.resource.taskservice.dal.repository.TaskRepository;

/**
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskDAL
 * @see TaskDB
 * @see TaskRepository
 * @see TaskRelationDB
 * @see TaskRelationRepository
 * @see TaskListDB
 * @see TaskListRepository
 * @see TaskDALQueries
 *
 */
@Component
public class TaskDALImpl implements TaskDAL {

	private static final Logger log = LoggerFactory.getLogger(TaskDALImpl.class);

	private TaskRepository taskRepository;
	private TaskRelationRepository taskRelationRepositiory;
	private TaskReminderRepository taskReminderRepository;
	private TaskListRepository taskListRepository;
	private TaskDALQueries q;

	@Autowired
	public TaskDALImpl(final TaskRepository taskRepository, final TaskRelationRepository taskRelationRepository,
			final TaskReminderRepository taskReminderRepository, final TaskListRepository taskListRepository,
			final TaskDALQueries q) {
		this.taskRepository = taskRepository;
		this.taskRelationRepositiory = taskRelationRepository;
		this.taskReminderRepository = taskReminderRepository;
		this.taskListRepository = taskListRepository;
		this.q = q;
	}

	@Override
	public List<TaskDB> loadTasksOnDay(final Date day, final Long userId) throws PRPException {

		if (day == null || userId == null) {
			String message = "Day and user ID is not given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		GregorianCalendar gregCal = new GregorianCalendar();
		gregCal.setTime(day);
		GregorianCalendar min = new GregorianCalendar();
		GregorianCalendar max = new GregorianCalendar();

		min.set(Calendar.DAY_OF_MONTH, gregCal.get(Calendar.DAY_OF_MONTH));
		min.set(Calendar.MONTH, gregCal.get(Calendar.MONTH));
		min.set(Calendar.YEAR, gregCal.get(Calendar.YEAR));
		min.set(Calendar.HOUR_OF_DAY, 0);
		min.set(Calendar.MINUTE, 0);
		min.set(Calendar.SECOND, 0);
		max.set(Calendar.DAY_OF_MONTH, gregCal.get(Calendar.DAY_OF_MONTH));
		max.set(Calendar.MONTH, gregCal.get(Calendar.MONTH));
		max.set(Calendar.YEAR, gregCal.get(Calendar.YEAR));
		max.set(Calendar.HOUR_OF_DAY, 23);
		max.set(Calendar.MINUTE, 59);
		max.set(Calendar.SECOND, 59);

		// @formatter:off
		List<TaskDB> tasksOnDay = taskRepository.findAll(Specification
				.where(q.ownedBy(userId))
				.and(q.after(min.getTime()))
				.and(q.before(max.getTime()))
				.and(q.notDone())
				.and(q.notDeleted())
				);
		// @formatter:on

		return tasksOnDay;
	}

	@Override
	public List<TaskDB> loadTasksDueOnDay(final Date day, final Long userId) throws PRPException {
		if (day == null || userId == null) {
			String message = "Day and user ID is not given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		GregorianCalendar gregCal = new GregorianCalendar();
		gregCal.setTime(day);
		GregorianCalendar min = new GregorianCalendar();
		GregorianCalendar max = new GregorianCalendar();

		min.set(Calendar.DAY_OF_MONTH, gregCal.get(Calendar.DAY_OF_MONTH));
		min.set(Calendar.MONTH, gregCal.get(Calendar.MONTH));
		min.set(Calendar.YEAR, gregCal.get(Calendar.YEAR));
		min.set(Calendar.HOUR_OF_DAY, 0);
		min.set(Calendar.MINUTE, 0);
		min.set(Calendar.SECOND, 0);
		max.set(Calendar.DAY_OF_MONTH, gregCal.get(Calendar.DAY_OF_MONTH));
		max.set(Calendar.MONTH, gregCal.get(Calendar.MONTH));
		max.set(Calendar.YEAR, gregCal.get(Calendar.YEAR));
		max.set(Calendar.HOUR_OF_DAY, 23);
		max.set(Calendar.MINUTE, 59);
		max.set(Calendar.SECOND, 59);

		// @formatter:off
		List<TaskDB> tasksOnDay = taskRepository.findAll(Specification
				.where(q.ownedBy(userId))
				.and(q.dueAfter(min.getTime()))
				.and(q.dueBefore(max.getTime()))
				.and(q.notDone())
				.and(q.notDeleted())
				);
		// @formatter:on

		return tasksOnDay;
	}

	@Override
	public List<TaskDB> loadTasksInRange(final Date start, final Date end, final Long userId) throws PRPException {
		if (start == null || end == null || userId == null) {
			String message = "Day and user ID is not given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		GregorianCalendar startCal = new GregorianCalendar();
		startCal.setTime(start);
		GregorianCalendar endCal = new GregorianCalendar();
		endCal.setTime(end);
		GregorianCalendar min = new GregorianCalendar();
		GregorianCalendar max = new GregorianCalendar();

		min.set(Calendar.DAY_OF_MONTH, startCal.get(Calendar.DAY_OF_MONTH));
		min.set(Calendar.MONTH, startCal.get(Calendar.MONTH));
		min.set(Calendar.YEAR, startCal.get(Calendar.YEAR));
		min.set(Calendar.HOUR_OF_DAY, 0);
		min.set(Calendar.MINUTE, 0);
		min.set(Calendar.SECOND, 0);
		max.set(Calendar.DAY_OF_MONTH, endCal.get(Calendar.DAY_OF_MONTH));
		max.set(Calendar.MONTH, endCal.get(Calendar.MONTH));
		max.set(Calendar.YEAR, endCal.get(Calendar.YEAR));
		max.set(Calendar.HOUR_OF_DAY, 23);
		max.set(Calendar.MINUTE, 59);
		max.set(Calendar.SECOND, 59);

		// @formatter:off
		List<TaskDB> tasksInRange = taskRepository.findAll(Specification
				.where(q.ownedBy(userId))
				.and(q.after(min.getTime()))
				.and(q.before(max.getTime()))
				.and(q.notDone())
				.and(q.notDeleted())
				);
		// @formatter:on

		return tasksInRange;
	}

	@Override
	public List<TaskDB> loadTasksDueInRange(final Date start, final Date end, final Long userId) throws PRPException {
		if (start == null || end == null || userId == null) {
			String message = "Day and user ID is not given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		GregorianCalendar startCal = new GregorianCalendar();
		startCal.setTime(start);
		GregorianCalendar endCal = new GregorianCalendar();
		endCal.setTime(end);
		GregorianCalendar min = new GregorianCalendar();
		GregorianCalendar max = new GregorianCalendar();

		min.set(Calendar.DAY_OF_MONTH, startCal.get(Calendar.DAY_OF_MONTH));
		min.set(Calendar.MONTH, startCal.get(Calendar.MONTH));
		min.set(Calendar.YEAR, startCal.get(Calendar.YEAR));
		min.set(Calendar.HOUR_OF_DAY, 0);
		min.set(Calendar.MINUTE, 0);
		min.set(Calendar.SECOND, 0);
		max.set(Calendar.DAY_OF_MONTH, endCal.get(Calendar.DAY_OF_MONTH));
		max.set(Calendar.MONTH, endCal.get(Calendar.MONTH));
		max.set(Calendar.YEAR, endCal.get(Calendar.YEAR));
		max.set(Calendar.HOUR_OF_DAY, 23);
		max.set(Calendar.MINUTE, 59);
		max.set(Calendar.SECOND, 59);

		// @formatter:off
		List<TaskDB> dueTasks = taskRepository.findAll(Specification
				.where(q.ownedBy(userId))
				.and(q.dueAfter(min.getTime()))
				.and(q.dueBefore(max.getTime()))
				.and(q.notDone())
				.and(q.notDeleted())
				);
		// @formatter:on

		return dueTasks;
	}

	@Override
	public List<TaskDB> loadOverdueTasks(final Date day, final Long userId) throws PRPException {
		if (day == null || userId == null) {
			String message = "Day and user ID is not given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		GregorianCalendar gregCal = new GregorianCalendar();
		gregCal.setTime(day);
		GregorianCalendar min = new GregorianCalendar();

		min.set(Calendar.DAY_OF_MONTH, gregCal.get(Calendar.DAY_OF_MONTH));
		min.set(Calendar.MONTH, gregCal.get(Calendar.MONTH));
		min.set(Calendar.YEAR, gregCal.get(Calendar.YEAR));
		min.set(Calendar.HOUR_OF_DAY, 0);
		min.set(Calendar.MINUTE, 0);
		min.set(Calendar.SECOND, 0);

		// @formatter:off
		List<TaskDB> overdue = taskRepository.findAll(Specification
				.where(q.ownedBy(userId))
				.and(q.overdue(min.getTime()))
				.and(q.notDone())
				.and(q.notDeleted())
				);
		// @formatter:on

		return overdue;
	}

	@Override
	public List<TaskDB> loadUnplannedTasks(final Long userId) throws PRPException {
		if (userId == null) {
			String message = "User ID is not given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		//@formatter:off
		List<TaskDB> tasks = taskRepository.findAll(Specification
				.where(q.ownedBy(userId))
				.and(q.noStartDate())
				.and(q.noDueDate())
				.and(q.notDone())
				.and(q.notDeleted()));
		//@formatter:on

		return tasks;
	}

	@Override
	public TaskDB loadTask(final Long id) throws PRPException {
		if (id == null) {
			String message = "Task ID is not given.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		Optional<TaskDB> found = taskRepository.findById(id);
		if (!found.isPresent()) {
			log.warn("No Task for given ID found.");
			return null;
		}

		return found.get();
	}

	@Override
	public TaskDB storeTask(final TaskDB task) throws PRPException {
		checkInput(task);

		taskRepository.save(task);
		return task;
	}

	@Override
	public TaskDB updateTask(final TaskDB task) throws PRPException {
		checkInput(task);

		Optional<TaskDB> foundTask = taskRepository.findById(task.getId());
		checkTaskExistence(foundTask);

		taskRepository.save(task);
		return task;
	}

	@Override
	public void deleteTask(final Long taskId) throws PRPException {
		Optional<TaskDB> foundTask = taskRepository.findById(taskId);
		checkTaskExistence(foundTask);

		foundTask.get().setDeleted(Boolean.TRUE);
		taskRepository.save(foundTask.get());
	}

	@Override
	public TaskReminderDB storeReminder(final TaskReminderDB reminder) throws PRPException {
		if (reminder == null || reminder.getTask() == null) {
			String message = "Task reminder to store oder task is missing.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}
		checkTaskExistence(taskRepository.findById(reminder.getTask().getId()));
		return taskReminderRepository.save(reminder);
	}

	@Override
	public void deleteReminder(final Long reminderId) throws PRPException {
		if (reminderId == null) {
			String message = "Task reminder ID missing.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		Optional<TaskReminderDB> foundReminder = taskReminderRepository.findById(reminderId);

		if (!foundReminder.isPresent()) {
			String message = "The reminder to delete doesn't exsist.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_TASK_REMINDER_NOT_EXISTANT);
		}

		TaskReminderDB reminder = foundReminder.get();
		reminder.setDeleted(Boolean.TRUE);

		taskReminderRepository.save(reminder);
	}

	@Override
	public TaskRelationDB storeRelation(final TaskRelationDB relation) throws PRPException {
		if (relation == null || relation.getTaskFrom() == null || relation.getTaskTo() == null
				|| relation.getRelation() == null || relation.getRelation().isEmpty()) {
			String message = "Relation information or tasks missing.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		checkTaskExistence(taskRepository.findById(relation.getTaskFrom().getId()));
		checkTaskExistence(taskRepository.findById(relation.getTaskTo().getId()));

		return taskRelationRepositiory.save(relation);
	}

	@Override
	public void deleteRelation(final Long relationId) throws PRPException {
		if (relationId == null) {
			String message = "Task relation ID missing.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}

		Optional<TaskRelationDB> found = taskRelationRepositiory.findById(relationId);
		if (!found.isPresent()) {
			String message = "The task to update doen't exist.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_TASK_RELATION_NOT_EXISTANT);
		}

		TaskRelationDB taskRelation = found.get();
		taskRelation.setDeleted(Boolean.TRUE);

		taskRelationRepositiory.save(taskRelation);
	}

	private void checkInput(final TaskDB task) throws PRPException {
		if (task == null || task.getTaskList() == null || task.getCreatorId() == null) {
			String message = "Task to store is missing.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING);
		}
		Optional<TaskListDB> foundTaskList = taskListRepository.findById(task.getTaskList().getId());
		if (!foundTaskList.isPresent()) {
			String message = "The desired task list doesn't exist.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_TASK_LIST_NOT_EXISTANT);
		}
	}

	private void checkTaskExistence(final Optional<TaskDB> foundTask) throws PRPException {
		if (!foundTask.isPresent()) {
			String message = "The task to update doen't exist.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_TASKSERVICE_DAL_TASK_NOT_EXISTANT);
		}
	}

}
