/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/**
 * Representing reminders for tasks.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskDB
 *
 */
@Entity
@Table(name = "lia_backend_taskservice_reminder", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "task", "remindtime" }) })
@Data
public class TaskReminderDB {

	@Id
	@GeneratedValue
	@Setter(AccessLevel.NONE)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "task", nullable = false)
	private TaskDB task;
	@Column(name = "remindtime", nullable = false, unique = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date remindTime;
	@Column(name = "deleted", nullable = false)
	private Boolean deleted = Boolean.FALSE;
}
