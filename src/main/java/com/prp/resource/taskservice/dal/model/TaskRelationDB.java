/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.dal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/**
 * Representing relations between tasks.
 * <p>
 * At the moment, only the {@link #BEFORE} relation is supported.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskDB
 *
 */
@Entity
@Table(name = "lia_backend_taskservice_taskrelations", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "fromTask", "toTask" }) })
@Data
public class TaskRelationDB {

	private static final Logger log = LoggerFactory.getLogger(TaskRelationDB.class);

	/**
	 * Task A has to be finished before task B.
	 *
	 * @since 0.1
	 */
	public static final String BEFORE = "before";

	@Id
	@GeneratedValue
	@Setter(AccessLevel.NONE)
	private Long id;
	@ManyToOne
	@JoinColumn(name = "fromTask", nullable = false)
	private TaskDB taskFrom;
	@ManyToOne
	@JoinColumn(name = "toTask", nullable = false)
	private TaskDB taskTo;
	@Column(name = "relation", nullable = false, unique = false)
	private String relation;
	@Column(name = "deleted", nullable = false)
	private Boolean deleted = Boolean.FALSE;

	public void setRelation(final String relation) {
		if (!BEFORE.equals(relation)) {
			String message = "The relation you wanted to store isn't valid.";
			log.error(message);
			throw new IllegalArgumentException(message);
		}
		this.relation = relation;
	}
}
