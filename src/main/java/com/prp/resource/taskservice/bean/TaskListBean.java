/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.bean;

import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.task.TaskList;
import com.prp.resource.taskservice.dal.TaskListDAL;

/**
 * Bean for managing task list operations.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see TaskList
 * @see TaskListDAL
 * @see Task
 *
 */
public interface TaskListBean {

	/**
	 * Creates a {@link TaskList}.
	 * <p>
	 * Tasks have to be created with the {@link TaskBean#addTask(Task, Long)} method and will be ignored here. If a {@link TaskList} should be
	 * created as a sublist of a {@link Task}, use {@link #addSubList(Long, TaskList)}.
	 *
	 * @param title - The {@link String} title for the list to create.
	 * @return {@link TaskList} - The created list.
	 * @throws PRPException
	 */
	TaskList createTaskList(String title) throws PRPException;

	/**
	 * At the moment, with this method only updating the name of a {@link TaskList} is possible.
	 * <p>
	 * The contained tasks have to be updated with {@link TaskBean#updateTask(Task)}.
	 *
	 * @param list - The {@link TaskList} to update.
	 * @return {@link TaskList} - The updated list.
	 * @throws PRPException
	 */
	TaskList updateTaskList(TaskList list) throws PRPException;

	/**
	 * Creates a sublist under a given {@link Task}.
	 *
	 * @param taskId - The {@link Long} identifying the {@link Task} to add the {@link TaskList} to.
	 * @param list   - The {@link TaskList} to create.
	 * @return {@link TaskList} - The created list.
	 * @throws PRPException
	 */
	TaskList addSubList(Long taskId, TaskList list) throws PRPException;

	/**
	 * Deletes a {@link TaskList} and all contained {@link Task}s.
	 *
	 * @param listId - The {@link Long} identifying the {@link TaskList} to delete.
	 * @throws PRPException
	 */
	void deleteTaskList(Long listId) throws PRPException;

	/**
	 * Loads all task list of the currently logged in user.
	 *
	 * @return {@link List}&lt;{@link TaskList}&gt;
	 * @throws PRPException
	 */
	List<TaskList> loadListsForUser() throws PRPException;

	/**
	 * Loads all sublists of the given task.
	 *
	 * @param taskId - The {@link Long} identifying the {@link Task} to load the {@link TaskList} for.
	 * @return {@link List}&lt;{@link TaskList}&gt; - All found lists for the given task.
	 * @throws PRPException
	 */
	List<TaskList> loadSubLists(Long taskId) throws PRPException;

}
