/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.bean;

import java.util.Date;
import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.task.Task;
import com.prp.resource.common.model.task.TaskList;
import com.prp.resource.taskservice.dal.TaskDAL;

/**
 * Bean for managing task operations.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see Task
 * @see TaskDAL
 *
 */
public interface TaskBean {

	/**
	 * Adds a new {@link Task} to the specified {@link TaskList}.
	 * <p>
	 * No sublists or relations/predecessor will be added.
	 *
	 * @param task - The {@link Task} to store.
	 * @return {@link Task} - The created task.
	 * @throws PRPException
	 */
	Task addTask(Task task, Long listId) throws PRPException;

	/**
	 * Adds a new Sub-{@link Task} to {@link Task}. The Tasks will be added to the default list (which is created when it doesn't already exists)
	 * <p>
	 * @param subTask The Sub-{@link Task} to store.
	 * @param parentTaskId Id of the parent task to which the subtask should be added
	 * @return {@link Task} - The created subtask.
	 * @throws PRPException
	 */
	Task addSubTask(Task subTask, Long parentTaskId) throws PRPException;

	/**
	 * Changes the {@link Task} information.
	 * <p>
	 * Predecessors won't be updated here, use {@link #addPredecessor(Long, Long)}.
	 *
	 * @param task - the {@link Task} to update.
	 * @return {@link Task} - The update task.
	 * @throws PRPException
	 */
	Task updateTask(Task task) throws PRPException;

	/**
	 * Marks the {@link Task} identified by the ID as done.
	 *
	 * @param taskId - The {@link Long} identifying the {@link Task}.
	 * @return {@link Task} - The updated task.
	 * @throws PRPException
	 */
	Task markAsDone(Long taskId) throws PRPException;

	/**
	 * Marks the {@link Task} identified by the ID as undone.
	 *
	 * @param taskId - The {@link Long} identifying the {@link Task}.
	 * @return {@link Task} - The updated task.
	 * @throws PRPException
	 */
	Task markAsUndone(Long taskId) throws PRPException;

	/**
	 * Marks the {@link Task}, identified by the ID, as done.
	 *
	 * @param taskId - The {@link Long} identifying the resource.
	 * @throws PRPException
	 */
	void deleteTask(Long taskId) throws PRPException;

	/**
	 * Adds the task relation {@code BEFORE} between the predecessor {@link Task} and the given {@link Task}. This means, the predecessor has to
	 * be completed, before the {@link Task} can be started.
	 *
	 * @param taskId        - The {@link Long} identifying the origin task.
	 * @param predecessorId - The {@link Long} identifying the predecessor task.
	 * @throws PRPException
	 */
	void addPredecessor(Long taskId, Long predecessorId) throws PRPException;

	/**
	 * Loads all tasks which are planned or due for the given day.
	 *
	 * @param day            - The {@link Date} to load the tasks for. Time information isn't relevant.
	 * @param includeOverdue - A {@code boolean} flag if overdue tasks should be included or not.
	 * @return {@link List}&lt;{@link Task}&gt; - The {@link Task}s planned on the given day or overdue tasks.
	 * @throws PRPException
	 */
	List<Task> loadTasksOnDay(Date day, boolean includeOverdue) throws PRPException;

	/**
	 * Loads all tasks planned or due for the week. Overdue tasks are not included.
	 *
	 * @param dateInWeek  - A {@link Date} representing the week to load the tasks for.
	 * @param startAtDate - A {@code boolean} flag marking if the search for tasks should at the given day or not. If {@code true} and the given
	 *                    date is a Wednesday, only tasks planned or due from Wednesday to Sunday are searched. If {@code false}, all tasks from
	 *                    Monday to Sunday are searched.
	 * @return
	 * @throws PRPException
	 */
	List<Task> loadTasksInWeek(Date dateInWeek, boolean startAtDate) throws PRPException;

	/**
	 * Loads all tasks for the logged in user that are not planned.
	 *
	 * @return {@link List}&lt;{@link Task}&gt; - The unplanned tasks.
	 * @throws PRPException
	 */
	List<Task> loadUnplannedTasks() throws PRPException;

	/**
	 * Loads a single task by it's ID.
	 *
	 * @param taskId - The {@link Long} representing the ID.
	 * @return
	 * @throws PRPException
	 */
	Task loadTask(Long taskId) throws PRPException;

}
