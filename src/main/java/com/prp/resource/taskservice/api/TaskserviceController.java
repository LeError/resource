/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.taskservice.api;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.task.Task;
import com.prp.resource.common.model.task.TaskList;
import com.prp.resource.exception.api.pojo.ApiExceptionResponse;
import com.prp.resource.taskservice.api.converter.ApiTaskConverter;
import com.prp.resource.taskservice.api.pojo.ApiTask;
import com.prp.resource.taskservice.api.pojo.ApiTaskList;
import com.prp.resource.taskservice.bean.TaskBean;
import com.prp.resource.taskservice.bean.TaskListBean;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

/**
 * Interface for accessing task functions.
 *
 * @author Eric Fischer
 * @see TaskBean
 * @see TaskListBean
 * @since 0.1
 */
@RestController
@RequestMapping("/tasks")
public class TaskserviceController {

    private static final String URL_DATE_FORMAT_STRING = "dd-MM-yyyy";
    private static final SimpleDateFormat URL_DATE_FORMAT = new SimpleDateFormat(
            URL_DATE_FORMAT_STRING);

    private TaskListBean taskListBean;
    private TaskBean taskBean;
    private ApiTaskConverter converter;

    @Autowired
    public TaskserviceController(final TaskBean taskBean, final TaskListBean taskListBean,
                                 final ApiTaskConverter converter) {
        this.taskBean = taskBean;
        this.taskListBean = taskListBean;
        this.converter = converter;
    }

    @Operation(
            summary = "Create a fresh task list.",
            description = "This endpoint creates a new task list with the given title. The task list created here can not contain tasks.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 201",
                    description = "The list was created successfuly.",
                    content = @Content(
                            schema = @Schema(implementation = ApiTaskList.class),
                            mediaType = "JSON")),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_B_PM",
                    description = "The parameter title is missing.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400 [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            }))
    })
    @PostMapping("/list")
    public ResponseEntity<ApiTaskList> createTaskList(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The title for the new task list.",
            content = @Content(examples = {
                    @ExampleObject(value = "Make PRP great")
            })) @RequestBody final String title) throws PRPException {
        return new ResponseEntity<>(converter.convertTaskList(taskListBean.createTaskList(title)),
                HttpStatus.CREATED);
    }

    @Operation(
            summary = "Update the task list's information.",
            description = "This endpoints updates the title of the specified task list. To update the contained tasks, you have to use the task's endpoints.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 200",
                    description = "The task list has been updated successfuly.",
                    content = @Content(
                            schema = @Schema(implementation = ApiTaskList.class),
                            mediaType = "JSON")),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_B_PM",
                    description = "The parameter title is missing.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "You are not allowed to change the task list. At the moment, only the owner of the task list is allowed to change it.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            }))
    })
    @PutMapping("/list/{id}")
    public ResponseEntity<ApiTaskList> updateTaskList(
            @Parameter(example = "1") @PathVariable("id") final Long id,
            @io.swagger.v3.oas.annotations.parameters.RequestBody(
                    description = "The new title for the task list",
                    content = @Content(
                            examples = @ExampleObject(
                                    value = "Make PRP great"))) @RequestBody final String title)
            throws PRPException {
        TaskList list = new TaskList();
        list.setId(id);
        list.setTitle(title);

        return new ResponseEntity<>(converter.convertTaskList(taskListBean.updateTaskList(list)),
                HttpStatus.OK);
    }

    public TaskList addSubList(final Long taskId, final TaskList list) throws PRPException {
        return taskListBean.addSubList(taskId, list);
    }

    @Operation(
            summary = "Delete an existing task list",
            description = "Marks a task list as deleted. This endpoint does not trigger a physical deletion.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 204",
                    description = "The task list was marked as deleted successfuly."),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_D_PM",
                    description = "The task list ID is not set (should not pop up, since this would indicate a bug).",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "You are not allowed to delete the task list. At the moment, only the owner of the task list is allowed to delete it.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 404 / B_T_D_TLNE",
                    description = "There's no task list for the given ID.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 404, \n [...] \n \"internalError\": \"B_T_D_TLNE\" \n }")
                            }))
    })
    @DeleteMapping("/list/{id}")
    public ResponseEntity<Void> deleteTaskList(
            @Parameter(example = "1") @PathVariable("id") final Long listId) throws PRPException {
        taskListBean.deleteTaskList(listId);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Operation(
            summary = "Load all task lists",
            description = "Load all tasks lists for the logged in user.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "HTTP 200", description = "All tasks have been loaded."),
            @ApiResponse(
                    responseCode = "HTTP 404",
                    description = "No task lists have been found. The user should create one.")
    })
    @GetMapping("/list")
    public ResponseEntity<List<ApiTaskList>> loadTaskListsForUser() throws PRPException {
        List<ApiTaskList> retVal = new ArrayList<>();
        for (TaskList toConvert : taskListBean.loadListsForUser()) {
            retVal.add(converter.convertTaskList(toConvert));
        }
        if (!retVal.isEmpty()) {
            return new ResponseEntity<>(retVal, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(
            summary = "Create a task",
            description = "Create a task within a specific task list.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 201",
                    description = "Task has been created and added to the specified list."),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "Creation of the task failed because the user is not allowed to edit the specified task list.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 404 / B_T_D_TLNE",
                    description = "The specified task list does not exist.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 404, \n [...] \n \"internalError\": \"B_T_D_TLNE\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_B_TSO",
                    description = "The start date of the task is after the due date. When creating tasks, the start date must be before the due date.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_TSO\" \n }")
                            }))
    })
    @PostMapping("/list/{listId}/task/create")
    public ResponseEntity<ApiTask> addTask(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The data of the task to add. "
                    + "The done flag, ID and the sublists are ignored here and have to be set with the corresponding endpoint. "
                    + "Please note, if the task list the freshly created task belongs in has a parent task (in other words: is a sublist)"
                    + ", the new task will be added as a predecessor to the parent task.") @RequestBody final ApiTask task,
                                           @PathVariable("listId") @Parameter(
                                                   description = "The ID of the task list to add the task to.") final Long listId)
            throws ParseException, PRPException {
        return new ResponseEntity<>(
                converter.convertTask(taskBean.addTask(converter.convertTask(task), listId)),
                HttpStatus.CREATED);
    }

    @Operation(summary = "Create a subtask", description = "Creates a subtask for a provided Task without needing a" +
            " sublist provided. Subtasks will be added to the default sublist of the Task. If the default list doesn't " +
            "exist it will be created by the operation.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 201",
                    description = "Subtask has been created and added to the default list."),
            @ApiResponse(
                    responseCode = "HTTP 404 / B_T_D_TNE",
                    description = "The specified parent task does not exist.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 404, \n [...] \n \"internalError\": \"B_T_D_TNE\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_B_TSO",
                    description = "The start date of the task is after the due date. When creating tasks, the start " +
                            "date must be before the due date.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_TSO\" \n }")
                            }))
    })
    @PostMapping("/{id}/task/create")
    public ResponseEntity<ApiTask> addSubtask(@Parameter(description = "The ID of the task to update.",
            example = "1") @PathVariable("id") Long parentTaskId, @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "The data of the subtask to add. "
            + "The done flag, ID and the sublists are ignored here and have to be set with the corresponding endpoint. "
            + "Please note, the new task will be added as a predecessor to the parent task.") @RequestBody ApiTask subtask) throws ParseException, PRPException {
        return new ResponseEntity<>(converter.convertTask(taskBean.addSubTask(converter.convertTask(subtask), parentTaskId)), HttpStatus.CREATED);
    }

    @Operation(
            summary = "Update a task's information",
            description = "Update a task specified by it's ID. If you just need "
                    + "to mark a task as done or undone, refer to the corresponding endpoints.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 200",
                    description = "The task has been updated an the updated values are returned.",
                    content = @Content(schema = @Schema(implementation = ApiTask.class))),
            @ApiResponse(
                    responseCode = "HTTP 400",
                    description = "One or more dates in the Task have been not formatted right. The right format is "
                            + ApiTask.DATE_FORMAT),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_D_PM",
                    description = "The task ID is not set (should not pop up, since this would indicate a bug).",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 404 / B_T_B_TNE",
                    description = "No task was found for the given ID. The ID from the request body is ignored here.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 404, \n [...] \n \"internalError\": \"B_T_B_TNE\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "Update of the task failed because the user is not allowed to edit the specified task.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            }))
    })
    @PutMapping("/{id}")
    public ResponseEntity<ApiTask> updateTask(@io.swagger.v3.oas.annotations.parameters.RequestBody(
            description = "The data to store. The ID is ignored, as this is taken from the path.") @RequestBody final ApiTask task,
                                              @PathVariable("id") @Parameter(
                                                      description = "The ID of the task to update.",
                                                      example = "1") final Long taskId)
            throws ParseException, PRPException {
        Task convertedTask = converter.convertTask(task);
        convertedTask.setId(taskId);
        return new ResponseEntity<>(converter.convertTask(taskBean.updateTask(convertedTask)),
                HttpStatus.OK);
    }

    @Operation(
            summary = "Quickly mark a task as done.",
            description = "This endpoint is a shortcut for marking tasks as done. "
                    + "Instead of updating the whole task you can just call this endpoint to avoid unneccessary traffic.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 200",
                    description = "The task has been updated successfuly."),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_D_PM",
                    description = "The task ID is not set (should not pop up, since this would indicate a bug).",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 404 / B_T_B_TNE",
                    description = "No task was found for the given ID. The ID from the request body is ignored here.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 404, \n [...] \n \"internalError\": \"B_T_B_TNE\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "Update of the task failed because the user is not allowed to edit the specified task.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            }))
    })
    @PutMapping("/{id}/done")
    public ResponseEntity<ApiTask> markAsDone(@Parameter(
            description = "The ID of the task to mark as done.",
            example = "1") @PathVariable("id") final Long taskId) throws PRPException {
        return new ResponseEntity<>(converter.convertTask(taskBean.markAsDone(taskId)),
                HttpStatus.OK);
    }

    @Operation(
            summary = "Quickly mark a task as undone.",
            description = "This endpoint is a shortcut for marking tasks as undone. "
                    + "Instead of updating the whole task you can just call this endpoint to avoid unneccessary traffic.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 200",
                    description = "The task has been updated successfuly."),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_D_PM",
                    description = "The task ID is not set (should not pop up, since this would indicate a bug).",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 404 / B_T_B_TNE",
                    description = "No task was found for the given ID. The ID from the request body is ignored here.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 404, \n [...] \n \"internalError\": \"B_T_B_TNE\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "Update of the task failed because the user is not allowed to edit the specified task.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            }))
    })
    @PutMapping("/{id}/undone")
    public ResponseEntity<ApiTask> markAsUndone(@PathVariable("id") final Long taskId)
            throws PRPException {
        return new ResponseEntity<>(converter.convertTask(taskBean.markAsUndone(taskId)),
                HttpStatus.OK);
    }

    @Operation(
            summary = "Delete a task",
            description = "Deletes the task specified by it's ID. "
                    + "Caution: No physical deletion is made here. This method just marks a task as deleted.")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "HTTP 204",
                    description = "The task has been marked as deleted successfuly."),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_D_PM",
                    description = "The task ID is not set (should not pop up, since this would indicate a bug).",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 404 / B_T_B_TNE",
                    description = "No task was found for the given ID.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 404, \n [...] \n \"internalError\": \"B_T_B_TNE\" \n }")
                            })),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "Update of the task failed because the user is not allowed to edit the specified task.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            }))
    })
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteTask(@Parameter(
            description = "The ID of the task to delete.",
            example = "1") @PathVariable("id") final Long taskId) throws PRPException {
        taskBean.deleteTask(taskId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public void addPredecessor(final Long taskId, final Long predecessorId) throws PRPException {
        taskBean.addPredecessor(taskId, predecessorId);
    }

    @Operation(
            summary = "Fetch tasks planned for the specified day.",
            description = "Fetch planned or due tasks for the specified day. Overdue tasks can be included"
                    + " optionally, e.g. for displaying all tasks that should be done today.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "HTTP 200", description = "Returns the filtered tasks."),
            @ApiResponse(
                    responseCode = "HTTP 404",
                    description = "No tasks found for the given day."),
            @ApiResponse(
                    responseCode = "HTTP 400",
                    description = "The date parameter is malformatted and could not be parsed."),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_D_PM",
                    description = "The date is not set (should not occur, otherwise we have a bug).",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
    })
    @GetMapping("/day/{dateString}")
    public ResponseEntity<List<ApiTask>> loadTasksOnDay(
            @Parameter(
                    description = "The day to fetch the tasks for in the format "
                            + URL_DATE_FORMAT_STRING,
                    example = "15-11-2020") @PathVariable(
                    name = "dateString") final String dayString,
            @Parameter(
                    description = "A flag if overdue tasks should be included. Default is 'false'.",
                    example = "true") @RequestParam(
                    name = "includeOverdue",
                    defaultValue = "false",
                    required = false) final boolean includeOverdue)
            throws ParseException, PRPException {
        Date day = URL_DATE_FORMAT.parse(dayString);
        List<Task> tasksOnDay = taskBean.loadTasksOnDay(day, includeOverdue);
        List<ApiTask> mappedTasksOnDay = tasksOnDay.stream()
                .map(task -> converter.convertTask(task)).collect(Collectors.toList());
        if (mappedTasksOnDay.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(mappedTasksOnDay, HttpStatus.OK);
    }

    @Operation(
            summary = "Fetch tasks within a week",
            description = "Fetch tasks planned or due within a week. "
                    + "The date handed to this endpoint may be any day within the "
                    + "week to fetch the tasks for. "
                    + "With an optional parameter you can tell the endpoint if it should fetch all "
                    + "tasks in the specified week starting at the specified day.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "HTTP 200", description = "Returns the filtered tasks."),
            @ApiResponse(
                    responseCode = "HTTP 404",
                    description = "No tasks found for the given day."),
            @ApiResponse(
                    responseCode = "HTTP 400",
                    description = "The date parameter is malformatted and could not be parsed."),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_D_PM",
                    description = "The date is not set (should not occur, otherwise we have a bug).",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
    })
    @GetMapping("/week/{dateString}")
    public ResponseEntity<List<ApiTask>> loadTasksInWeek(
            @Parameter(
                    description = "The day in the week to fetch the tasks for in the format "
                            + URL_DATE_FORMAT_STRING,
                    example = "15-11-2020") @PathVariable("dateString") final String dateString,
            @Parameter(
                    description = "Determines, if the server should tasks starting at "
                            + "the date or if it should fetch all tasks due or planned within "
                            + "the week the handed date is in.",
                    example = "false") @RequestParam(
                    name = "startAtDate",
                    defaultValue = "true",
                    required = false) final boolean startAtDate)
            throws ParseException, PRPException {
        Date dateInWeek = URL_DATE_FORMAT.parse(dateString);
        List<Task> tasksInWeek = taskBean.loadTasksInWeek(dateInWeek, startAtDate);
        List<ApiTask> mappedTasksInWeek = tasksInWeek.stream()
                .map(task -> converter.convertTask(task)).collect(Collectors.toList());
        if (mappedTasksInWeek.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(mappedTasksInWeek, HttpStatus.OK);
    }

    @Operation(
            summary = "Load tasks without a planned and a due date.",
            description = "Loads all tasks, that have neither a due date not a planned date.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "HTTP 200", description = "Returns the filtered tasks."),
            @ApiResponse(
                    responseCode = "HTTP 404",
                    description = "No tasks found for the given day."),
            @ApiResponse(
                    responseCode = "HTTP 400",
                    description = "The date parameter is malformatted and could not be parsed."),
            @ApiResponse(
                    responseCode = "HTTP 400 / B_T_D_PM",
                    description = "The date is not set (should not occur, otherwise we have a bug).",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 400, \n [...] \n \"internalError\": \"B_T_B_PM\" \n }")
                            })),
    })
    @GetMapping("/unplanned")
    public ResponseEntity<List<ApiTask>> loadUnplannedTasks() throws PRPException {
        List<Task> unplannedTasks = taskBean.loadUnplannedTasks();
        List<ApiTask> mappedUnplannedTasks = unplannedTasks.stream()
                .map(task -> converter.convertTask(task)).collect(Collectors.toList());
        if (mappedUnplannedTasks.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(mappedUnplannedTasks, HttpStatus.OK);
    }

    @Operation(
            summary = "Load Sub-Lists for specified TaskId",
            description = "Loads all Sub-Lists for a specified TaskId. Only Lists owned by the user will be returned")
    @ApiResponses({
            @ApiResponse(responseCode = "HTTP 200", description = "Returns the filtered Sublists."),
            @ApiResponse(
                    responseCode = "HTTP 400",
                    description = "The TaskId Parameter is Missing"),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "Fetching the Sublists failed because the user is not allowed to access the specified task / list.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            }))
    })
    @GetMapping("/{id}/sub-list")
    public List<TaskList> loadSubLists(@PathVariable("id") final Long id) throws PRPException {
        return taskListBean.loadSubLists(id);
    }

    @Operation(
            summary = "Load Task for specified TaskId",
            description = "Loads the Task for the specified TaskId if the User Requesting it is the Task Owner")
    @ApiResponses({
            @ApiResponse(responseCode = "HTTP 200", description = "Returns the filtered tasks."),
            @ApiResponse(
                    responseCode = "HTTP 400",
                    description = "The TaskId Parameter is Missing"),
            @ApiResponse(
                    responseCode = "HTTP 403 / B_T_B_WUI",
                    description = "Fetching the Task failed because the user is not allowed to access the specified task.",
                    content = @Content(
                            mediaType = "JSON",
                            schema = @Schema(implementation = ApiExceptionResponse.class),
                            examples = {
                                    @ExampleObject(
                                            value = "{ \n \"status\": 403, \n [...] \n \"internalError\": \"B_T_B_WUI\" \n }")
                            }))
    })
    @GetMapping("/{id}")
    public Task loadTask(@PathVariable("id") final Long id) throws PRPException {
        return taskBean.loadTask(id);
    }

}
