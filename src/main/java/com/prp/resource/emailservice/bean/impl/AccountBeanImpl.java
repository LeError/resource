/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.bean.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.User;
import com.prp.resource.common.model.email.account.Account;
import com.prp.resource.common.model.email.account.EmailAddress;
import com.prp.resource.common.model.email.account.connection.Connection;
import com.prp.resource.common.service.UserService;
import com.prp.resource.emailservice.bean.AccountBean;
import com.prp.resource.emailservice.dal.AccountDal;
import com.prp.resource.emailservice.dal.model.AccountDB;
import com.prp.resource.emailservice.dal.model.EmailAddressDB;
import com.prp.resource.emailservice.dal.model.HostAccessDB;
import com.prp.resource.emailservice.dal.model.HostDB;
import com.prp.resource.emailservice.util.AccountConverter;
import com.prp.resource.emailservice.util.EmailAddressConverter;

/**
 *
 * @author Eric Fischer
 * @since 0.1
 * @see AccountBean
 *
 */
@Component
public class AccountBeanImpl implements AccountBean {

	private static Logger log = LoggerFactory.getLogger(AccountBean.class);

	private UserService userService;
	private AccountDal accountDal;
	private AccountConverter accountConverter;

	@Autowired
	public AccountBeanImpl(final UserService userService, final AccountDal accountDal,
			final AccountConverter accountConverter) {
		this.userService = userService;
		this.accountDal = accountDal;
		this.accountConverter = accountConverter;
	}

	@Override
	public Account registerAccount(final Account account) throws PRPException {

		if (account.getConnectionMethods() == null || account.getConnectionMethods().isEmpty()) {
			throwLiaAccountCreationException();
		} else {
			for (Connection connection : account.getConnectionMethods().values()) {
				if (connectionInformationMissing(connection)) {
					throwLiaAccountCreationException();
				}
			}
		}

		AccountDB accountDb = new AccountDB();
		convertToAccountDB(account, accountDb);

		accountDal.storeAccount(accountDb);

		return accountConverter.convertAccount(accountDb);
	}

	private boolean connectionInformationMissing(final Connection connection) {
		return connection.getHost() == null || connection.getHost().isEmpty() || connection.getPort() == null
				|| connection.getPort().equals(Integer.valueOf(0)) || connection.getMethod() == null
				|| connection.getUsername() == null || connection.getUsername().isEmpty()
				|| connection.getPassword() == null || connection.getPassword().isEmpty();
	}

	private void throwLiaAccountCreationException() throws PRPException {
		String message = "Account can't be created because of missing inforamtion.";
		log.error(message);
		throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_ACCOUNT_CRIDENTIALS_NOT_COMPLETE);
	}

	@Override
	public List<Account> findAccountsForCurrentUser() throws PRPException {

		User user = userService.loadCurrentUser();

		List<AccountDB> accounts = accountDal.loadAccountsForUser(user.getId());

		if (accounts == null) {
			log.warn("No account for current user found.");
			return null;
		}

		List<Account> retVal = new ArrayList<>();

		for (AccountDB accountDB : accounts) {
			retVal.add(accountConverter.convertAccount(accountDB));
		}

		return retVal;
	}

	@Override
	public Account updateAccount(final Account account) throws PRPException {
		if (account == null || account.getId() == null || account.getId().equals(Long.valueOf(0))) {
			String message = "No account given to update.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_ACCOUNT_INFORMATION_NOT_COMPLETE);
		}

		AccountDB loaded = accountDal.loadAccount(account.getId());

		if (loaded == null) {
			String message = "No account for given information found.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_ACCOUNT_NOT_FOUND);
		}

		convertToAccountDB(account, loaded);

		AccountDB updated = accountDal.updateAccount(loaded);

		return accountConverter.convertAccount(updated);
	}

	@Override
	public Account loadAccount(final Long id) throws PRPException {
		AccountDB accountDB = accountDal.loadAccount(id);
		Account accountFound = accountConverter.convertAccount(accountDB);
		return accountFound;
	}

	private void convertToAccountDB(final Account account, final AccountDB accountDb) throws PRPException {
		accountDb.setName(account.getName());
		String primaryFetchingMethod = "";
		switch (account.getPrimaryFetchingMethod()) {
		case POP3:
			primaryFetchingMethod = AccountDB.POP3;
			break;
		case IMAP:
			primaryFetchingMethod = AccountDB.IMAP;
			break;
		default:
			String message = "The request method within the account is not known to the system.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_FETCHING_METHOD_NOT_SUPPORTED);
		}
		accountDb.setPrimaryFetchingMethod(primaryFetchingMethod);
		accountDb.setUserID(userService.loadCurrentUser().getId());
		accountDb.getHosts().clear();
		for (Connection connection : account.getConnectionMethods().values()) {
			HostDB host = accountDal.loadHostInformation(connection.getHost(), connection.getMethod());
			if (host == null) {
				host = new HostDB();
				host.setHostUrl(connection.getHost());
				host.setPort(connection.getPort());
				host.setMethod(connection.getMethod());
			}
			HostAccessDB hostAccess = null;
			if (host.getId() != null && account.getId() != null) {
				hostAccess = accountDal.loadHostAccessInformation(host.getId(), account.getId());
			}
			if (hostAccess == null) {
				hostAccess = new HostAccessDB();
			}
			hostAccess.setHost(host);
			hostAccess.setPassword(connection.getPassword());
			hostAccess.setUsername(connection.getUsername());
			hostAccess.getEmailAddresses().clear();
			for (EmailAddress emailAddress : account.getAddresses()) {
				EmailAddressDB found = accountDal.loadEmailAddress(emailAddress.getPublicName(),
						emailAddress.getEmail());
				if (found == null) {
					hostAccess.getEmailAddresses().add(EmailAddressConverter.convertEmailAddress(emailAddress));
				} else {
					hostAccess.getEmailAddresses().add(found);
				}
			}
			accountDb.getHosts().add(hostAccess);
		}
	}

}
