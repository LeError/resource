/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.bean;

import java.util.List;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.common.model.email.Email;
import com.prp.resource.common.model.email.account.Account;
import com.prp.resource.common.model.email.account.EmailAddress;
import com.prp.resource.emailservice.service.MailService;

/**
 * This bean is responsible for email managing. Functions like sending, receiving, deleting, moving e.g. are located here.
 * <p>
 * For sending and receiving emails, the {@link MailService} is called. If using IMAP, also deleting and moving emails will effect a call on
 * this service.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see Email
 * @see Account
 * @see EmailAddress
 * @see MailService
 */
public interface EmailBean {

	/**
	 * Method for sending emails. The used protocol has to be set to the {@link Account} within the {@link Email}.
	 * <p>
	 * This method typically calls the {@link MailService#sendMail(Email)} and stores the email into the database, if it should.
	 *
	 * @param email     - The {@link Email} to send. Remember to set all important information like the {@link Account} and the sending
	 *                  {@link EmailAddress}.
	 * @param storeToDb - If <code>true</code>, the email will be stored to the database. <code>false</code> only is useful, if internal
	 *                  messages like user activation emails are send. All emails send by users should have <code>true</code> here.
	 * @throws PRPException
	 * @since 0.1
	 */
	void sendMail(Email email, boolean storeToDb) throws PRPException;

	/**
	 * Lists emails stored for the given {@link Account}. Furthermore, a fetch on the provider for new emails is performed.
	 * <p>
	 * The method (POP3, IMAP) the emails are fetched by is choosen from the given {@link Account}.
	 * <p>
	 * <strong>Important notice</strong><br>
	 * This method only fetches a certain number of emails which should be given. Furthermore, a start number has to be handed to the method.
	 * Both are necessary to perform fetches dynamically and reduce loading times. So if you want to load all emails in the inbox, call this
	 * function with
	 * <p>
	 * <code>
	 * fetchEmails(myAccount, Integer.valueOf(20), Integer.valueOf(0));
	 * </code>
	 * <p>
	 * e.g. The important fact is that you use a start value of 0 to get all emails from the newest on. If the user wants to load more emails,
	 * just hand the <code>count</code> you entered before as the start value
	 * <p>
	 * <code>
	 * fetchEmails(myAccount, Integer.valueOf(20), Integer.valueOf(20));
	 * </code>
	 * <p>
	 * to load the next block of emails. How many emails you want to load may depend on the kind of device you are using or the quality of
	 * internet connection.
	 *
	 * @param account - The {@link Account} the emails should be fetched for.
	 * @param count   - The number of emails that should be fetched.
	 * @param start   - The start number the emails should be loaded from. This param allows you to load a large number of emails dynamically.
	 * @return {@link List}<{@link Email}> - The list with the emails fetched or stored.
	 * @throws PRPException
	 * @since 0.1
	 */
	List<Email> fetchEmails(Long accountId, EmailRequestMethod method, Integer count, Integer start)
			throws PRPException;

	Email loadEmail(Long id) throws PRPException;

}
