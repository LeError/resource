/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.util;

import com.prp.resource.common.model.email.account.EmailAddress;
import com.prp.resource.emailservice.dal.model.EmailAddressDB;

/**
 * Converts email address information between API and DB model.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see EmailAddressDB
 * @see EmailAddress
 *
 */
public class EmailAddressConverter {

	private EmailAddressConverter() {}

	public static EmailAddressDB convertEmailAddress(final EmailAddress address) {
		EmailAddressDB db = new EmailAddressDB();
		db.setAddress(address.getEmail());
		if (address.getPublicName() == null || address.getPublicName().isEmpty()) {
			db.setSenderName(address.getEmail());
		} else {
			db.setSenderName(address.getPublicName());
		}
		return db;
	}

	public static EmailAddress convertEmailAddress(final EmailAddressDB in) {
		EmailAddress out = new EmailAddress();
		out.setEmail(in.getAddress());
		if (in.getSenderName() == null || in.getSenderName().isEmpty()) {
			out.setPublicName(in.getAddress());
		} else {
			out.setPublicName(in.getSenderName());
		}
		return out;
	}

}
