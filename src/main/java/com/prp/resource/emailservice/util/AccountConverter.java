/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.enums.email.EmailRequestMethod;
import com.prp.resource.common.model.email.account.Account;
import com.prp.resource.common.model.email.account.EmailAddress;
import com.prp.resource.common.model.email.account.connection.IMAP;
import com.prp.resource.common.model.email.account.connection.POP3;
import com.prp.resource.common.model.email.account.connection.SMTP;
import com.prp.resource.emailservice.dal.model.AccountDB;
import com.prp.resource.emailservice.dal.model.EmailAddressDB;
import com.prp.resource.emailservice.dal.model.HostAccessDB;

/**
 * Converter for the whole account information.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see Account
 * @see AccountDB
 * @see EmailAddressConverter
 *
 */
@Component
public final class AccountConverter {

	private static final Logger log = LoggerFactory.getLogger(AccountConverter.class);

	public Account convertAccount(final AccountDB in) throws PRPException {
		Account out = new Account();

		out.setName(in.getName());
		out.setId(in.getId());
		switch (in.getPrimaryFetchingMethod()) {
		case "POP3":
			out.setPrimaryFetchingMethod(EmailRequestMethod.POP3);
			break;
		case "IMAP":
			out.setPrimaryFetchingMethod(EmailRequestMethod.IMAP);
			break;
		default:
			String message = "The request method in database is not known to the system.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_BEAN_FETCHING_METHOD_NOT_SUPPORTED);
		}

		for (HostAccessDB hostAccess : in.getHosts()) {
			switch (hostAccess.getHost().getMethod()) {
			case IMAP:
				IMAP imap = new IMAP();
				imap.setHost(hostAccess.getHost().getHostUrl());
				imap.setUsername(hostAccess.getUsername());
				imap.setPassword(hostAccess.getPassword());
				imap.setPort(hostAccess.getHost().getPort());
				out.getConnectionMethods().put(EmailRequestMethod.IMAP, imap);
				break;
			case POP3:
				POP3 pop3 = new POP3();
				pop3.setHost(hostAccess.getHost().getHostUrl());
				pop3.setUsername(hostAccess.getUsername());
				pop3.setPassword(hostAccess.getPassword());
				pop3.setPort(hostAccess.getHost().getPort());
				pop3.setLeaveCopyOnServer(hostAccess.isLeaveCopyOnServer());
				out.getConnectionMethods().put(EmailRequestMethod.POP3, pop3);
				break;
			case SMTP:
				SMTP smtp = new SMTP();
				smtp.setHost(hostAccess.getHost().getHostUrl());
				smtp.setUsername(hostAccess.getUsername());
				smtp.setPassword(hostAccess.getPassword());
				smtp.setPort(hostAccess.getHost().getPort());
				out.getConnectionMethods().put(EmailRequestMethod.SMTP, smtp);
				break;
			}

			for (EmailAddressDB emailAddressDb : hostAccess.getEmailAddresses()) {
				EmailAddress emailAddress = EmailAddressConverter.convertEmailAddress(emailAddressDb);
				out.getAddresses().add(emailAddress);
			}

		}

		return out;
	}

}
