/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.emailservice.dal.AccountDal;
import com.prp.resource.emailservice.dal.MailDal;
import com.prp.resource.emailservice.dal.model.AccountDB;
import com.prp.resource.emailservice.dal.model.EmailAddressDB;
import com.prp.resource.emailservice.dal.model.EmailDB;
import com.prp.resource.emailservice.dal.model.HostAccessDB;
import com.prp.resource.emailservice.dal.repository.EmailAddressRepository;
import com.prp.resource.emailservice.dal.repository.EmailRepository;

/**
 *
 * @author Eric Fischer
 * @since 0.1
 * @see MailDal
 *
 */
@Component
public class MailDalImpl implements MailDal {

	private static final Logger log = LoggerFactory.getLogger(MailDalImpl.class);

	private EmailRepository emailRepository;
	private AccountDal accountDal;
	private EmailAddressRepository emailAddressRepository;

	@Autowired
	public MailDalImpl(final EmailRepository emailRepository, final AccountDal accountDal,
			final EmailAddressRepository emailAddressRepository) {
		this.emailRepository = emailRepository;
		this.accountDal = accountDal;
		this.emailAddressRepository = emailAddressRepository;
	}

	@Override
	public void storeEmail(final EmailDB email) throws PRPException {

		if (email == null) {
			String message = "No email given to store.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_NO_EMAIL_TO_STORE);
		}

		HostAccessDB hostAccess = null;
		if (email.getHostAccess() != null) {

			log.debug("Checking account...");

			if (email.getHostAccess().getAccount() == null || email.getHostAccess().getAccount().getId() == null
					|| email.getHostAccess().getAccount().getId().equals(Long.valueOf(0))) {
				String message = "No account id given to search for in DB.";
				log.error(message);
				throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_ACCOUNT_ID_MISSING);
			}

			log.debug("Checking user ID.");

			if (email.getHostAccess().getAccount().getUserID() == null
					|| Long.valueOf(0).equals(email.getHostAccess().getAccount().getUserID())) {
				String message = "No user id given within the account.";
				log.error(message);
				throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_USER_ID_MISSING);
			}

			AccountDB account = email.getHostAccess().getAccount();

			log.debug("Loading account information from account DAL...");

			hostAccess = accountDal.loadHostAccessInformation(email.getHostAccess().getHost().getId(), account.getId());

			if (hostAccess == null) {
				String message = "No host access information found by account DAL.";
				log.error(message);
				throw new PRPException(message,
						PRPErrorCode.BACKEND_EMAILSERVICE_DAL_INFORMATION_ABOUT_HOSTACCESS_MISSING);
			}

			email.setHostAccess(hostAccess);

			log.debug("Loaded account information from account DAL. Storing email addresses...");

			Iterator<EmailAddressDB> iterator = email.getTo().iterator();
			List<EmailAddressDB> newTo = new ArrayList<>();
			while (iterator.hasNext()) {
				newTo.add(checkDatabaseEntries(iterator.next()));
			}
			email.setTo(newTo);

			iterator = email.getCc().iterator();
			List<EmailAddressDB> newCc = new ArrayList<>();
			while (iterator.hasNext()) {
				newCc.add(checkDatabaseEntries(iterator.next()));
			}
			email.setCc(newCc);

			iterator = email.getBcc().iterator();
			List<EmailAddressDB> newBcc = new ArrayList<>();
			while (iterator.hasNext()) {
				newBcc.add(checkDatabaseEntries(iterator.next()));
			}
			email.setBcc(newBcc);

			EmailAddressDB foundEmailAddress = emailAddressRepository.findBySenderNameAndAddress(
					email.getFrom().get(0).getSenderName(), email.getFrom().get(0).getAddress());

			if (foundEmailAddress != null) {
				email.getFrom().remove(0);
				email.getFrom().add(foundEmailAddress);
			}

			log.debug("Email addresses stored.");

		} else {
			String message = "No host access information given. In this case, there's no account to send the email with.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_INFORMATION_ABOUT_HOSTACCESS_MISSING);
		}

		log.debug("Storing email to database...");
		emailRepository.saveAndFlush(email);
		log.info("Email stored to database.");

	}

	private EmailAddressDB checkDatabaseEntries(EmailAddressDB emailAddressDB) {
		EmailAddressDB db = emailAddressRepository.findBySenderNameAndAddress(emailAddressDB.getSenderName(),
				emailAddressDB.getAddress());
		if (db != null) {
			emailAddressDB = db;
		} else {
			emailAddressRepository.save(emailAddressDB);
		}
		return emailAddressDB;
	}

	@Override
	public EmailDB loadEmail(final Long id) throws PRPException {
		EmailDB mailDB = emailRepository.getOne(id);
		return mailDB;

	}

	@Override
	public void deleteEmail(final Long id) throws PRPException {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteDraft(final Long id) throws PRPException {
		// TODO Auto-generated method stub

	}

	@Override
	public List<EmailDB> findEmailsForUser(final Long userId) throws PRPException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void storeDraft(final EmailDB email) throws PRPException {
		// TODO Auto-generated method stub

	}

	@Override
	public void editDraft(final EmailDB email) throws PRPException {
		// TODO Auto-generated method stub

	}

	@Override
	public EmailDB checkIfExistant(final String subject, final EmailAddressDB sender, final Date sentDate)
			throws PRPException {
		if (subject == null || sender == null || sentDate == null) {
			String message = "Parameters incomplete to search for an email.";
			log.error(message);
			throw new PRPException(message, PRPErrorCode.BACKEND_EMAILSERVICE_DAL_EMAIL_SEARCH_PARAMETER_MISSING);
		}

		List<EmailDB> found = emailRepository.findBySubjectAndSent(subject, sentDate);
		if (found.size() == 1) {
			return found.get(0);
		} else if (found.size() == 0) {

			return null;
		} else {
			for (EmailDB emailDB : found) {
				EmailAddressDB stored = emailDB.getFrom().get(0);
				if (stored.getAddress().equals(sender.getAddress())
						&& stored.getSenderName().equals(sender.getSenderName())) {
					return emailDB;
				}
			}
			return null;
		}
	}

}
