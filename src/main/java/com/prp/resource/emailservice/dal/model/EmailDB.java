/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.dal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "lia_backend_emailservice_email")
@Data
public class EmailDB {

	@Id @GeneratedValue @Setter(AccessLevel.NONE) private Long id;
	@Column(name = "subject", nullable = false, unique = false) private String subject;
	@OneToMany(mappedBy = "mail", cascade = CascadeType.ALL) private List<EmailContentBlockDB> content = new ArrayList<>();
	// @formatter:off
	@ManyToMany(cascade= {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name = "lia_backend_emailservice_sentfrom",
			joinColumns = {	@JoinColumn(name = "email") },
			inverseJoinColumns = { @JoinColumn(name = "address") })
	private List<EmailAddressDB> from = new ArrayList<>();
	@ManyToMany(cascade= {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name = "lia_backend_emailservice_sentto",
			joinColumns = {	@JoinColumn(name = "email") },
			inverseJoinColumns = { @JoinColumn(name = "address") })
	private List<EmailAddressDB> to = new ArrayList<>();
	@ManyToMany(cascade= {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name = "lia_backend_emailservice_sentcc",
			joinColumns = {	@JoinColumn(name = "email") },
			inverseJoinColumns = { @JoinColumn(name = "address") })
	private List<EmailAddressDB> cc = new ArrayList<>();
	@ManyToMany(cascade= {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	@JoinTable(
			name = "lia_backend_emailservice_sentbcc",
			joinColumns = {	@JoinColumn(name = "email") },
			inverseJoinColumns = { @JoinColumn(name = "address") })
	private List<EmailAddressDB> bcc = new ArrayList<>();
	// @formatter:on
	@Temporal(TemporalType.TIMESTAMP) @Column(name = "sentdate", nullable = true, unique = false) private Date sent;
	@Temporal(TemporalType.TIMESTAMP) @Column(name = "receiveddate", nullable = true, unique = false) private Date received;
	@Column(name = "deleted", nullable = false, unique = false) private Boolean deleted = Boolean.FALSE;
	@OneToMany(mappedBy = "mail", cascade = CascadeType.ALL) @ToString.Exclude @EqualsAndHashCode.Exclude private List<AppandixDB> appandicies = new ArrayList<>();
	@ManyToOne @JoinColumn(name = "hostid", nullable = false, unique = false) @ToString.Exclude private HostAccessDB hostAccess;

}
