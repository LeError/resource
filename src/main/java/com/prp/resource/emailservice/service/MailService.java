/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.emailservice.service;

import java.util.List;

import javax.mail.Message;

import com.prp.resource.common.exception.PRPException;
import com.prp.resource.common.model.email.Email;
import com.prp.resource.common.model.email.account.Account;

/**
 * Service responsible for sending and receiving emails. If IMAP is used, multiple methods have to be called here in order to keep
 * everything in sync.
 *
 * @author Eric Fischer
 * @since 0.1
 * @see Email
 * @see Account
 */
public interface MailService {

	/**
	 * This method is responsible for sending emails.
	 * <p>
	 * At the moment, TLS is enabled by default. Other parameters, like host, port and credentials, have to be provided within the
	 * {@link Email}.
	 *
	 * @param email - The {@link Email} to send. Remember to fill the required fields.
	 * @throws LiaException
	 */
	void sendMail(Email email) throws PRPException;

	List<Message> fetchMailsPOP3(Account account) throws PRPException;

	List<Message> fetchMailsIMAP(Account account) throws PRPException;
}
