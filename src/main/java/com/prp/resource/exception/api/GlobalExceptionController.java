/*
        PRP Project
        Copyright (C) 2020 The PRP Project

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.prp.resource.exception.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.prp.resource.common.enums.exception.PRPErrorCode;
import com.prp.resource.common.exception.PRPException;
import com.prp.resource.exception.api.pojo.ApiExceptionResponse;
import com.sun.el.parser.ParseException;

@ControllerAdvice
public class GlobalExceptionController extends ResponseEntityExceptionHandler {

	private static final String MALFORMED_DATE_REJECTED_BY_THE_ENDPOINT = "Malformed date rejected by the endpoint.";
	private static final Logger log = LoggerFactory.getLogger(GlobalExceptionController.class);

	@ExceptionHandler(PRPException.class)
	protected ResponseEntity<Object> handlePRPException(final PRPException ex,
			final WebRequest request) {
		HttpStatus httpStatus;
		switch (ex.getErrorCode()) {
			case BACKEND_EMAILSERVICE_BEAN_ACCOUNT_NOT_FOUND:
			case BACKEND_EMAILSERVICE_SERVICE_SENDER_ADDRESS_AND_NAME_NOT_SETABLE:
			case BACKEND_EMAILSERVICE_DAL_ACCOUNT_NOT_FOUND:
			case BACKEND_EMAILSERVICE_DAL_ACCOUNT_NAME_ALREADY_TAKEN:
			case BACKEND_CALENDARSERVICE_BEAN_END_BEFORE_START:
			case BACKEND_TASKSERVICE_BEAN_TASK_STARTS_OVERDUE:
			case BACKEND_TASKSERVICE_BEAN_PREDECESSOR_NOT_BEFORE_TASK:
			case BACKEND_TASKSERVICE_BEAN_ALL_SLOTS_FILLED:
			case BACKEND_TASKSERVICE_BEAN_PARAMETERS_MISSING:
			case BACKEND_TASKSERVICE_DAL_PARAMETERS_MISSING:
			case BACKEND_CALENDARSERVICE_BEAN_PARAMETERS_MISSING:
			case BACKEND_CALENDARSERVICE_BEAN_CALENDAR_NOT_GIVEN:
			case BACKEND_CALENDARSERVICE_DAL_USER_ALREADY_HAS_PERMISSION:
				httpStatus = HttpStatus.BAD_REQUEST;
				break;
			case BACKEND_CALENDARSERVICE_DAL_NO_EVENT_FOR_ID_FOUND:
			case BACKEND_TASKSERVICE_DAL_TASK_LIST_NOT_EXISTANT:
			case BACKEND_CALENDARSERVICE_DAL_NO_CALENDAR_FOR_ID_FOUND:
			case BACKEND_CALENDARSERVICE_BEAN_CALENDAR_NOT_FOUND:
			case BACKEND_CALENDARSERVICE_BEAN_NO_EVENT_FOR_ID_FOUND:
				httpStatus = HttpStatus.NOT_FOUND;
				break;
			case BACKEND_CALENDARSERVICE_BEAN_NO_PERMISSION_FOUND:
				httpStatus = HttpStatus.UNAUTHORIZED;
				break;
			case BACKEND_CALENDARSERVICE_BEAN_USER_HAS_NO_PERMISSION:
			case BACKEND_TASKSERVICE_BEAN_WRONG_USER_ID:
				httpStatus = HttpStatus.FORBIDDEN;
				break;
			case BACKEND_CALENDARSERVICE_DAL_CALENDAR_PERMISSION_DATA_NO_COMPLETE:
			case BACKEND_CALENDARSERVICE_DAL_CALENDAR_ID_MISSING:
			default:
				httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		ApiExceptionResponse exceptionResponse = ApiExceptionResponse.builder()
				.error(ex.getClass().getName()).message(ex.getMessage()).status(httpStatus.value())
				.path(((ServletWebRequest) request).getRequest().getRequestURI())
				.internalErrorCode(ex.getErrorCode()).build();
		return handleExceptionInternal(ex, exceptionResponse, new HttpHeaders(), httpStatus,
				request);
	}

	@ExceptionHandler(ParseException.class)
	public ResponseEntity<Object> handleParseExceptions(final Exception ex,
			final WebRequest request) {
		log.error("Malformed date contained in request.");
		ApiExceptionResponse exceptionResponse = ApiExceptionResponse.builder()
				.error(ex.getClass().getName()).message(MALFORMED_DATE_REJECTED_BY_THE_ENDPOINT)
				.status(HttpStatus.BAD_REQUEST.value())
				.path(((ServletWebRequest) request).getRequest().getRequestURI())
				.internalErrorCode(null).build();
		return handleExceptionInternal(ex, exceptionResponse, new HttpHeaders(),
				HttpStatus.BAD_REQUEST, request);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> defaultHandler(final Exception ex, final WebRequest request) {
		log.error(ex.getMessage(), ex);
		ApiExceptionResponse exceptionResponse = ApiExceptionResponse.builder()
				.error(ex.getClass().getName()).message(ex.getMessage())
				.status(HttpStatus.INTERNAL_SERVER_ERROR.value())
				.path(((ServletWebRequest) request).getRequest().getRequestURI())
				.internalErrorCode(PRPErrorCode.UNKNOWN).build();
		return handleExceptionInternal(ex, exceptionResponse, new HttpHeaders(),
				HttpStatus.INTERNAL_SERVER_ERROR, request);
	}

}
